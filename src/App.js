import React from 'react';
import Notes from './components/Notes';
import Header from './components/Header';
import axios from 'axios'
const baseUrl = '/api/persons'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      persons: [],
      newName: '',
      number: ''
    }
  }

  componentDidMount() {
    axios
      .get(baseUrl)
      .then(response => {
        this.setState({ persons: response.data })
      })
      .catch(error => {
        console.log('fail')
      })
  }

  addName = (event) => {
    event.preventDefault()
    if (this.state.persons.find(x => x.name === this.state.newName)) {

      alert('Nimi on jo listassa!')
      this.setState({
        newName:'',
        number:''
      })
      } else {  
      const nameObject = {
        name: this.state.newName,
        number: this.state.number,
    }


    axios.post(baseUrl, nameObject)
      .then(response => {
        this.setState({
          persons: this.state.persons.concat(response.data),
          newName: '',
          number: '',
        })
      })
    }
  }

  delName = (id) => {
    return () => {
      if (window.confirm('Haluatko varmasti poistaa tiedon?')) {
        window.location.reload(false);
        axios.delete(`${baseUrl}/${id}`)
          .then(response => {
          })
      }
    }
  }

  handleNameChange = (event) => {
    //console.log(event.target.value)
    this.setState({
      newName: event.target.value
    })
  }

  handleNumberChange = (event) => {
    //console.log(event.target.value)
    this.setState({
      number: event.target.value
    })
  }

  render() {
    return (
      <div>
        <Header />
        <form onSubmit={this.addName}>
          <div>
            nimi: <input value={this.state.newName} onChange={this.handleNameChange} />
          </div>
          <div>
            numero: <input value={this.state.number} onChange={this.handleNumberChange} />
          </div>
          <div>
            <button type="submit">lisää</button>
          </div>
        </form>
        <h2>Numerot</h2>
        <div>
          {this.state.persons.map(persons => <Notes key={persons.id} persons={persons} delName={this.delName(persons.id)} />)}


        </div>
      </div>
    )
  }
}

export default App